package adma.ui.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;

import adma.core.Adma;
import adma.core.devices.Device;
import adma.ui.utils.Colors;
import adma.ui.views.AdmaView;
import adma.ui.views.DevicePanel;

public class AdmaController {

	private AdmaView admaView;

	public AdmaController(AdmaView admaView) {
		this.admaView = admaView;
	}

	public void setActions(Adma adma, ArrayList<DevicePanel> devicesPanel) {
		for (DevicePanel devicePanel : devicesPanel) {
			devicePanel.getButton().addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					Device device = devicePanel.getDevice();
					adma.changeStatus(device);
				}
			});
		}
	}

	public void updateDevicePanel(Device device, String status, ArrayList<DevicePanel> devicesPanel) {
		for (DevicePanel devicePanel : devicesPanel) {
			if (devicePanel.getDevice().equals(device)) {
				updateButtonColorAndText(status, devicePanel);
			}
		}
	}

	private void updateButtonColorAndText(String status, DevicePanel devicePanel) {
		JButton button = devicePanel.getButton();

		button.setText(status);
		if (isON(status))
			button.setBackground(Colors.COLOR_ON);
		else
			button.setBackground(Colors.COLOR_OFF);
	}

	private boolean isON(String status) {
		return status.equals("ON");
	}

	public AdmaView getAdmaView() {
		return admaView;
	}

}
