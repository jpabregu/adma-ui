package adma.ui.views;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import adma.ui.utils.Colors;

public class NorthBar extends JPanel {

	private static final long serialVersionUID = 1L;

	private ImageIcon logo = new ImageIcon(this.getClass().getResource("/images/admaLogoMini.png"));

	public NorthBar() {
		super();
		setBackground(Colors.COLOR_BACKGROUND);

		ImagePanel panel_3 = new ImagePanel((logo).getImage());
		panel_3.setBounds(22, 72, 297, 122);
		panel_3.setBackground(Colors.COLOR_BACKGROUND_FRAME);
		add(panel_3);
	}
}
