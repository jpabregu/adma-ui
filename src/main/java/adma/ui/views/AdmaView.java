package adma.ui.views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JScrollPane;

import adma.core.Adma;
import adma.core.devices.Device;
import adma.core.devices.Status;
import adma.ui.controllers.AdmaController;
import adma.ui.utils.Colors;

@SuppressWarnings("deprecation")
public class AdmaView implements Observer {

	private final String TITLE = "ADMA - Tu Lugar";
	private Image icon = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/icon.png"));

	private JFrame frame;
	private JScrollPane scrollPane;
	private GraphPanel graphPanel;

	private Adma adma;
	private AdmaController controller;
	private ArrayList<DevicePanel> devicesPanel;

	public AdmaView(Adma adma) {
		this.adma = adma;
		adma.addObserver(this);
		this.devicesPanel = new ArrayList<>();

		this.controller = new AdmaController(this);
		setDevices(adma.getDevices());
		initialize();

		controller.setActions(adma, devicesPanel);
	}

	private void initialize() {
		frame = new JFrame();
		frame.setTitle(TITLE);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(50, 50, 500, 600);
		frame.setResizable(false);
		frame.setIconImage(icon);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		frame.getContentPane().setBackground(Colors.COLOR_BACKGROUND_FRAME);
		frame.getContentPane().add(new NorthBar(), BorderLayout.NORTH);
		frame.getContentPane().add(new SouthBar(), BorderLayout.SOUTH);

		setView();
	}

	private void setView() {
		graphPanel = new GraphPanel();
		FlowLayout flowLayout = (FlowLayout) graphPanel.getLayout();
		flowLayout.setVgap(0);
		flowLayout.setHgap(0);
		graphPanel.setAlignmentY(5.0f);
		graphPanel.add(new DevicesPanel(devicesPanel));

		scrollPane = new JScrollPane(graphPanel);
		scrollPane.setAlignmentY(5.0f);
		scrollPane.setOpaque(false);
		scrollPane.setBorder(null);
		scrollPane.getViewport().setOpaque(false);
		frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
	}

	private void setDevices(Map<Device, Status> devices) {
		for (Entry<Device, Status> entry : devices.entrySet()) {
			Device device = entry.getKey();
			String status = entry.getValue().toString();

			DevicePanel devicePanel = new DevicePanel(device, status, "/images/ligth.png");
			devicesPanel.add(devicePanel);

			controller.updateDevicePanel(device, status, devicesPanel);
		}
	}

	public void init() {
		frame.setVisible(true);
	}

	@Override
	public void update(Observable o, Object arg) {
		Device device = (Device) arg;
		String status = adma.getDeviceStatus(device);

		controller.updateDevicePanel(device, status, devicesPanel);
	}
}
