
package adma.ui.views;

import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

public class DevicesPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	public DevicesPanel(ArrayList<DevicePanel> devicesPanel) {
		setOpaque(false);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		addDevicePanel(devicesPanel);
	}

	private void addDevicePanel(ArrayList<DevicePanel> devicesPanel) {
		for (DevicePanel devicePanel : devicesPanel) {
			add(devicePanel);
		}
	}

}