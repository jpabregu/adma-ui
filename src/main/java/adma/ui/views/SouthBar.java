package adma.ui.views;

import javax.swing.JLabel;
import javax.swing.JPanel;

import adma.ui.utils.Colors;

public class SouthBar extends JPanel {

	private static final long serialVersionUID = 1L;

	private JLabel userName;

	public SouthBar() {
		super();
		setBackground(Colors.COLOR_BACKGROUND);
		userName = new JLabel();
		userName.setForeground(Colors.COLOR_SUBTITLE);
		add(userName);
	}
}
