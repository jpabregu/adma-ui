package adma.ui.views;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;

import adma.core.devices.Device;
import adma.ui.utils.Colors;

public class DevicePanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private JPanel panelDevice;
	private JLabel nameDevice;
	private JButton buttonDevice;

	private Device device;

	public DevicePanel(Device device, String status, String image) {
		super();
		this.device = device;

		setOpaque(false);
		setBorder(new CompoundBorder(new LineBorder(Colors.COLOR_BACKGROUND_FRAME, 10, true),
				new MatteBorder(1, 1, 0, 1, (Color) new Color(255, 255, 255))));

		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 32, 0, 0, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 1.0, 0.0, 4.0, 4.0, 1.0, 0.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		nameDevice = new JLabel(device.getName().toUpperCase());
		nameDevice.setHorizontalTextPosition(SwingConstants.CENTER);
		nameDevice.setHorizontalAlignment(SwingConstants.CENTER);
		nameDevice.setForeground(Color.WHITE);
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.gridheight = 3;
		gbc_lblNewLabel.fill = GridBagConstraints.VERTICAL;
		gbc_lblNewLabel.gridwidth = 2;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 0;
		add(nameDevice, gbc_lblNewLabel);

		ImageIcon fondo_tablero = new ImageIcon(this.getClass().getResource(image));
		panelDevice = new ImagePanel((fondo_tablero).getImage());

		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.gridheight = 2;
		gbc_panel.gridwidth = 2;
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 1;
		gbc_panel.gridy = 3;
		add(panelDevice, gbc_panel);

		buttonDevice = new JButton(status);
		buttonDevice.setFocusable(false);
		buttonDevice.setHorizontalTextPosition(SwingConstants.CENTER);
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnNewButton.gridwidth = 2;
		gbc_btnNewButton.gridx = 1;
		gbc_btnNewButton.gridy = 5;
		add(buttonDevice, gbc_btnNewButton);
	}

	public JButton getButton() {
		return buttonDevice;
	}

	public Device getDevice() {
		return device;
	}
}
