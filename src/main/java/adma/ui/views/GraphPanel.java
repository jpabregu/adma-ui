package adma.ui.views;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import adma.ui.utils.Colors;

class GraphPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	public GraphPanel() {
		setBackground(Colors.COLOR_BACKGROUND_PANEL);
	}

	public void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
	}

}