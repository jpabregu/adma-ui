package adma.ui.utils;

import java.awt.Color;

public class Colors {
	public static final Color COLOR_TITLE = Color.decode("#ffffff");
	public static final Color COLOR_SUBTITLE = Color.decode("#d3d3d3");

	public static final Color COLOR_BACKGROUND = Color.decode("#1176AE");
	public static final Color COLOR_BACKGROUND_PANEL = Color.decode("#168aad");
	public static final Color COLOR_BACKGROUND_FRAME = Color.decode("#18191D");

	public static final Color COLOR_BORDER = Color.decode("#277da1");

	public static final Color COLOR_OFF = Color.decode("#f94144");
	public static final Color COLOR_ON = Color.decode("#90be6d");
}
