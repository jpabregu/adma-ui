package adma.ui.main;

import adma.core.Adma;
import adma.ui.views.AdmaView;

public class Application {

	private Adma adma;
	private AdmaView admaView;

	public Application() {
		this.adma = new Adma();
		this.admaView = new AdmaView(adma);
	}

	private void init() {
		this.admaView.init();
	}

	public static void main(String[] args) {
		new Application().init();
	}

}
